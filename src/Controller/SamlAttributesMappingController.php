<?php

namespace Drupal\saml_attributes_mapping\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\field\Entity\FieldConfig;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Default controller for the saml_attributes_mapping module.
 */
class SamlAttributesMappingController extends ControllerBase {

  /**
   * Used to get field names for user entity.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityFieldManagerInterface $entityFieldManager) {
    $this->entityFieldManager = $entityFieldManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_field.manager')
    );
  }

  /**
   * Get the custom user fields.
   *
   * @return array
   *   A field array of custom user fields.
   */
  public function getCustomUserFields() {
    $fields = $this->entityFieldManager->getFieldDefinitions('user', 'user');
    $custom_fields = [];
    foreach ($fields as $field_name => $field) {
      if ($field instanceof FieldConfig) {
        $custom_fields[$field_name]['name'] = $field->getLabel();
        $custom_fields[$field_name]['type'] = $field->getType();
      }
    }
    return $custom_fields;
  }

  /**
   * Get the SAML attributes of user fields.
   *
   * @return array
   *   A custom user field array with the SAML attributes.
   */
  public function getSamlAttributes() {
    $config = $this->config('saml_attributes_mapping.settings');
    $mappings = $config->get('field_mapping');
    $saml_attributes = [];
    if (count($mappings)) {
      foreach ($mappings as $user_field => $saml_field) {
        if ($saml_field['attribute']) {
          $saml_attributes[$user_field] = $saml_field;
        }
      }
    }
    return $saml_attributes;
  }

}
