<?php

namespace Drupal\saml_attributes_mapping\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * {@inheritdoc}
 */
class AttributesFieldsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'saml_attributes_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'saml_attributes_mapping.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('saml_attributes_mapping.settings');
    $form['attributes_names'] = [
      '#type' => 'textarea',
      '#title' => $this->t('SimpleSAMLphp attributes'),
      '#description' => $this->t('Multiple values separated by pipe ("|") symbol. Ex: eduPersonName | displayName | mail.'),
      '#default_value' => $config->get('saml_attributes'),
    ];
    $form['create_taxonomy'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create taxonomy terms if not exists while SAML attributes are mapping'),
      '#default_value' => $config->get('create_taxonomy'),
      '#description' => $this->t('Note: Taxomy terms creates parent level only.'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('saml_attributes_mapping.settings');
    $config->set('saml_attributes', $form_state->getValue('attributes_names'));
    $config->set('create_taxonomy', $form_state->getValue('create_taxonomy'));
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
