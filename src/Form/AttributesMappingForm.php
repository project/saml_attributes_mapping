<?php

namespace Drupal\saml_attributes_mapping\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Utility\SafeMarkup;
use Drupal\saml_attributes_mapping\Controller\SamlAttributesMappingController;

/**
 * {@inheritdoc}
 */
class AttributesMappingForm extends ConfigFormBase {

  /**
   * SamlAttributesMappingController variable.
   *
   * @var object
   */
  protected $mappingController;

  /**
   * {@inheritdoc}
   */
  public function __construct(SamlAttributesMappingController $mappingController) {
    $this->mappingController = $mappingController;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('saml_attributes.mapping_controler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'saml_attributes_mapping';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'saml_attributes_mapping.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('saml_attributes_mapping.settings');
    $custom_fields = $this->mappingController->getCustomUserFields();
    $attributes = [];
    $attributes[''] = '-None-';
    $saml_attributes = explode('|', $config->get('saml_attributes'));
    foreach ($saml_attributes as $saml_field) {
      $saml_field = trim($saml_field);
      if ($saml_field) {
        $attributes[$saml_field] = $saml_field;
      }
    }

    $header = [
      'custom_field' => $this->t('Custom User field'),
      'attribute' => $this->t('SAML attribute'),
      'field_type' => '',
    ];
    $field_mapping = $config->get('field_mapping');
    $form['mapping'] = [
      '#type' => 'table',
      '#header' => $header,
    ];

    if (empty($custom_fields)) {
      $form['mapping']['#empty'] = $this->t('There are no custom fields.');
      unset($form['actions']);
    }
    else {
      if (count($attributes) === 1) {
        $add_item = Url::fromRoute('saml_attributes_mapping.settings_attributes');
        $message = $this->t('There are no SAML attributes configured yet.');
        $form['mapping']['#empty'] = SafeMarkup::format($message . ' ' . $this->l($this->t('Add an item'), $add_item), []);
        unset($form['actions']);
      }
      else {
        foreach ($custom_fields as $field_key => $filed) {
          $form['mapping'][$field_key]['custom_field']['#markup'] = $filed['name'];
          $form['mapping'][$field_key]['attribute'] = [
            '#type' => 'select',
            '#options' => $attributes,
            '#default_value' => isset($field_mapping[$field_key]) ? $field_mapping[$field_key]['attribute'] : '',
          ];
          $form['mapping'][$field_key]['field_type'] = [
            '#type' => 'hidden',
            '#value' => $filed['type'],
          ];
        }
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $mapping = $form_state->getValue('mapping');
    $this->configFactory->getEditable('saml_attributes_mapping.settings')
      ->set('field_mapping', $mapping)
      ->save();
    parent::submitForm($form, $form_state);
  }

}
