SimpleSAMLphp Attributes Mapping - Introduction
-----------------------------------------------

This module allows to mapping the simpleSAMLphp attributes 
to Drupal user fields when user is login through SAML.


REQUIREMENTS
------------

This module requires the following modules:
 
 * simpleSAMLphp Authentication (https://drupal.org/project/simplesamlphp_auth)


Installation
------------
 * Copy the saml_attributes_mapping folder to your module directory.
 * At Administer -> Modules (admin/modules) enable the module.


Configuration
--------------
 * Configure the module settings page link
   - Administer -> Configuration -> People -> SimpleSAMLphp Auth Settings -> SAML attributes. 
   - (admin/config/people/saml_attributes).
 * SAML attributes user field mapping page link
   - Administer -> Configuration -> People -> SimpleSAMLphp Auth Settings -> SAML attributes mapping.
   - (admin/config/people/mapping_fields).


Maintainers
-----------
Selva Gajendran.S
